package net.npaka.instantgram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AppMyPage extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.mypage);
    }

    public void move_option(View view) {
        Intent intent = new Intent(this, AppOptionMenu.class);
        startActivity(intent);
    }

    public void shoot_camera(View view) {
        Intent intent = new Intent(this, AppPost.class);
        startActivity(intent);
    }

    public void move_main(View view) {
        Intent intent = new Intent(this, AppMainActivity.class);
        startActivity(intent);
    }
    public void move_search(View view) {
        Intent intent = new Intent(this, AppSearch.class);
        startActivity(intent);
    }
    public void move_mypage(View view) {
        Intent intent = new Intent(this, AppMyPage.class);
        startActivity(intent);
    }
}
