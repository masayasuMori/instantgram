package net.npaka.instantgram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AppOptionDetail extends AppCompatActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.profile);
    }

    //戻るボタン押下時に呼ばれる
    public void go_back(View view) {
        Intent intent = new Intent(this, AppOptionMenu.class);
        startActivity(intent);
    }

    //変更ボタン押下時に呼ばれる
    public void change_status(View view) {
        Intent intent = new Intent(this, AppMyPage.class);
        startActivity(intent);
    }
}
