package net.npaka.instantgram;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class InstantOpenHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "instantDB";
    public static final int DB_VERSION = 1;

    public static final String CREATE_TABLE =
            "create table if not exists " + InstantContract.Login.TABLE_NAME + "(" +
                    InstantContract.Login._ID + " integer primary key autoincrement, " +
                    InstantContract.Login.COL_ID + " TEXT, " +
                    InstantContract.Login.COL_PASS + " TEXT," +
                    InstantContract.Login.SAVE_LOGIN + " TEXT)";
    public static final String INIT_TABLE =
            "insert into " + InstantContract.Login.TABLE_NAME + "(" +
                    InstantContract.Login.COL_ID + ", " +
                    InstantContract.Login.COL_PASS + ", " +
                    InstantContract.Login.SAVE_LOGIN + ")" +
                    " values " +
                    "('t1', 'b1', '0')";

    public InstantOpenHelper(Context c) {
        super(c, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        //db.execSQL(INIT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}
