package net.npaka.instantgram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class AppOptionMenu extends AppCompatActivity {
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.option);

        ListView optionList = (ListView) findViewById(R.id.options);
        optionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(AppOptionMenu.this, AppLogout.class);
                    startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(AppOptionMenu.this, AppOptionDetail.class);
                    startActivity(intent);
                }
            }
        });
    }

    public void back_home(View view) {
        Intent intent = new Intent(this, AppMyPage.class);
        startActivity(intent);
    }
}
