package net.npaka.instantgram;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class LoginFormActivity extends Activity {

    public static final String AUTHORITY =
            "net.npaka.instantgram.LoginContentProvider";
    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY + "/" + InstantContract.Login.TABLE_NAME);
    public static final Uri CONTENT =
            Uri.parse("content://" + AUTHORITY + "/");


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.login);
    }

    //ログインボタン押下時に呼ばれる
    public void login(View v) {
        //テキストボックスの取得
        TextView textId = findViewById(R.id.login_id);
        TextView textPass = findViewById(R.id.login_pass);
        CheckBox saveInfo = findViewById(R.id.save_login);

        //テキストの取得
        String loginId = textId.getText().toString();
        String loginPass = textPass.getText().toString();

        //ログイン IDとPassが一致しているかチェックし、IDとPassを取得する
        Cursor c = this.getContentResolver().query(CONTENT_URI, new String[]{InstantContract.Login.COL_ID, InstantContract.Login.COL_PASS},
                InstantContract.Login.COL_ID + "=" + loginId + " AND " + InstantContract.Login.COL_PASS + "=" + loginPass
                , null, null);

        //取得した行の数を取得
        int rows = c.getCount();
        if (rows == 0) {
            //エラー表示
            toast("ID もしくは パスワードが間違っています");
        } else {
            ContentValues values = new ContentValues();
            //保存するかどうかDBを更新
            if (saveInfo.isChecked() == true) {
                values.put(InstantContract.Login.SAVE_LOGIN, 1);
            } else {
                values.put(InstantContract.Login.SAVE_LOGIN, 0);
            }

            //保存するかを更新
            getContentResolver().update(CONTENT_URI, values, InstantContract.Login.COL_ID + "=" + loginId + " AND " + InstantContract.Login.COL_PASS + "=" + loginPass, null);
            //メインページに遷移 マイページのデータ引渡しはできていない。
            Intent intent = new Intent(this, AppMainActivity.class);
            startActivity(intent);
        }
        c.close();
    }

    //新規登録ボタン押下時に呼ばれる
    public void Registration(View v) {
        TextView textId = findViewById(R.id.login_id);
        TextView textPass = findViewById(R.id.login_pass);
        CheckBox saveInfo = findViewById(R.id.save_login);

        //テキストの取得
        String loginId = textId.getText().toString();
        String loginPass = textPass.getText().toString();

        //コンテンツプロバイダが提供するDBへのアクセス
        ContentValues values = new ContentValues();
        values.put(InstantContract.Login.COL_ID, loginId);
        values.put(InstantContract.Login.COL_PASS, loginPass);

        //チェックボックスがチェックされているか
        if (saveInfo.isChecked()) {
            values.put(InstantContract.Login.SAVE_LOGIN, 1);
        } else {
            values.put(InstantContract.Login.SAVE_LOGIN, 0);
        }

        //ログイン IDが一致しているかチェックし、IDを取得する
        /*
        Cursor c = this.getContentResolver().query(CONTENT, new String[]{InstantContract.Login.COL_ID},
                InstantContract.Login.COL_ID + "=" + loginId,
                null, null);
*/
        Cursor c = this.getContentResolver().query(CONTENT, new String[]{InstantContract.Login.COL_ID},
                InstantContract.Login.COL_ID + " = " +loginId,
                null, null);

        //同じIDが無いか
        if (c.getCount() == 0) {
            //DBに挿入
            getContentResolver().insert(CONTENT_URI, values);

            //メインページに遷移 マイページのデータ引渡しはできていない。
            Intent intent = new Intent(this, AppMainActivity.class);
            startActivity(intent);
        } else {
            toast("このIDはすでに使用されています。");
        }
        c.close();
    }


    //トーストの表示
    private void toast(String text) {
        if (text == null) text = "";
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
