package net.npaka.instantgram;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AppLogout extends AppCompatActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.logout);
    }

    //戻るボタン押下時に呼ばれる
    public void go_back(View view) {
        Intent intent = new Intent(this, AppOptionMenu.class);
        startActivity(intent);
    }

    //ログアウトボタン押下時に呼ばれる
    public void logout(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
