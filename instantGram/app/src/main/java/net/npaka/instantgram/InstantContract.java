package net.npaka.instantgram;

import android.provider.BaseColumns;

public final class InstantContract {

    public InstantContract() {}

    public static abstract class Login implements BaseColumns {
        public static final String TABLE_NAME = "loginName";
        public static final String COL_ID = "loginId";
        public static final String COL_PASS = "loginPass";
        public static final String SAVE_LOGIN = "autoLogin";
    }



}
