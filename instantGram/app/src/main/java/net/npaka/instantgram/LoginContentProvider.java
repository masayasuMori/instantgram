package net.npaka.instantgram;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class LoginContentProvider extends ContentProvider {
    SQLiteDatabase db;


    //UriMatcherに登録する
    private static final int LOGINS = 1;
    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH); //-1が返ってくる
        //addURI(AUTHORITY, テーブル名、返り値)
        uriMatcher.addURI(MainActivity.AUTHORITY, InstantContract.Login.TABLE_NAME + "/1", LOGINS);
    }

    private InstantOpenHelper instantOpenHelper;

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        db.insert(InstantContract.Login.TABLE_NAME, "", values);
        return uri;
    }

    @Override
    public boolean onCreate() {
        instantOpenHelper = new InstantOpenHelper(getContext());
        db = instantOpenHelper.getWritableDatabase();
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor c = db.query(
                InstantContract.Login.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        return db.update(InstantContract.Login.TABLE_NAME, values, null, null);
    }
}
