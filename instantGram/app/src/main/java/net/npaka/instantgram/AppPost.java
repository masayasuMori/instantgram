package net.npaka.instantgram;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


public class AppPost extends AppCompatActivity {
    private final static int RESULT_CAMERA = 1001;  //カメラリクエストコード
    private final static int RESULT_GALLERY = 2002; //ギャラリーリクエストコード
    private final static int REQUEST_PERMISSIONS = 3003; //ギャラリーリクエストコード
    private ImageView imageView;    //イメージビューの宣言文
    private EditText editText;
    private Uri resultUri;
    LocationManager manager;
    private final static String BR = System.getProperty("line.separator");
    private final static String[] PERMISSIONS = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.post);

        imageView = findViewById(R.id.postImage);

        //撮影ボタン　カメラ
        Button takePicture = findViewById(R.id.takePicture);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, RESULT_CAMERA);
            }
        });

        //ギャラリー選択ボタン
        Button selectPicture = findViewById(R.id.selectPicture);
        selectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery;
                intentGallery = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intentGallery.addCategory(Intent.CATEGORY_OPENABLE);
                intentGallery.setType("image/jpeg");
            }
        });

        //現在地の取得ボタン
        Button nowLocation = findViewById(R.id.nowLocation);
        nowLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText = findViewById(R.id.location);
                takeLocation();
            }
        });
    }

    //緯度経度の取得
    private void takeLocation() {

        //ロケーションマネージャの取得
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //ユーザーの利用許可のチェック
        checkPermissions();
    }

    @Override //再開時に呼ばれる
    public void onResume() {
        super.onResume();
        //位置情報更新の開始
        setLocationUpdateEnabled(true);
    }

    //位置情報の開始・停止
    private void setLocationUpdateEnabled(boolean enabled) {
        //パーミッション
        if (!isGranted()) {
            return;
        }

        //ロケーションマネージャの解除と登録
        try {
            if (enabled) {
                manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) AppPost.this);
            } else {
                manager.removeUpdates((LocationListener) AppPost.this);
            }
        } catch (SecurityException e) {
        }
    }

    //ユーザーの利用許可チェック
    private void checkPermissions() {
        //未許可
        if (!isGranted()) {
            //許可ダイアログの表示
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);
        }
    }

    //ユーザーの利用許可が済かどうかの取得
    private boolean isGranted() {
        for (int i = 0; i < PERMISSIONS.length; i++) {
            if (PermissionChecker.checkSelfPermission(
                    AppPost.this, PERMISSIONS[i]) !=
                            PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    //許可ダイアログ選択時に呼ばれる
    @Override
    public void onRequestPermissionsResult(int requestCode, String permission[], int[] results) {
        if (requestCode == REQUEST_PERMISSIONS) {
            //未許可
            if (!isGranted()) {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permission, results);
        }
    }

    //ImageViewに撮った写真を張り付け
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CAMERA) {
            Bitmap bitmap;
            bitmap = (Bitmap) data.getExtras().get("data");

            //画像サイズを計測
            int bmpWidth = bitmap.getWidth();
            int bmpHeight = bitmap.getHeight();
            //貼り付け
            imageView.setImageBitmap(bitmap);
        } else if (requestCode == RESULT_GALLERY) {
            MediaScannerConnection.scanFile(
                    this,
                    new String[]{resultUri.getPath()},
                    new String[]{"image/jpeg"},
                    null
            );

            //画像を設定
            imageView.setImageURI(resultUri);
        }
    }

    //メイン画面に戻る
    public void back_appmain(View view) {
        Intent intent = new Intent(this, AppMainActivity.class);
        startActivity(intent);
    }

    //投稿する
    public void post_appmain(View view) {
        Intent intent = new Intent(this, AppMainActivity.class);
        startActivity(intent);
    }
}
