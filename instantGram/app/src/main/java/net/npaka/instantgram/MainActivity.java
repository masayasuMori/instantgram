package net.npaka.instantgram;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public Cursor c;

    public static final String AUTHORITY =
            "net.npaka.instantgram.LoginContentProvider";
    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY + "/" + InstantContract.Login.TABLE_NAME);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        int param = readDB();

        if (param == 0) {
            Intent intent = new Intent(this, LoginFormActivity.class);
            startActivity(intent);
        } else if (param > 0) {
            Intent intent = new Intent(this, AppMainActivity.class);
            startActivity(intent);
        }

    }

    //データベースからの読み込み
    private int readDB() {
        Cursor c = this.getContentResolver().query(CONTENT_URI, new String[]{InstantContract.Login.SAVE_LOGIN},
                "autoLogin='1'", null, null);
        int param = c.getCount();
        c.close();
        return param;
    }
}